#################################################################
# HEADER
#################################################################

CMAKE_MINIMUM_REQUIRED(VERSION 2.8.11)
SET(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS TRUE)

PROJECT(FPL Fortran)

SET(${PROJECT_NAME}_VERSION 0.0.1)
SET(${PROJECT_NAME}_SOVERSION 1)

#################################################################
# DEFINE PATHS
#################################################################

SET(ROOT_PATH ${CMAKE_SOURCE_DIR}   )        # FPL root path 
SET(SRC_PATH ${ROOT_PATH}/src)               # FPL Sources path 
SET(THIRDPARTY_PATH ${ROOT_PATH}/thirdparty) # FPL ThirdParty path 

#################################################################
# CONFIGURATION TYPES & BUILD MODE & TESTING
#################################################################

SET(CMAKE_CONFIGURATION_TYPES DEBUG RELEASE) # Specifies the available build types
IF(NOT CMAKE_BUILD_TYPE)                     # If build type was not specified, set DEBUG as default
  SET(CMAKE_BUILD_TYPE DEBUG CACHE STRING
      "Choose the type of build, options are: NONE DEBUG RELEASE"
      FORCE)

  SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS NONE DEBUG RELEASE)
ENDIF(NOT CMAKE_BUILD_TYPE)

IF(NOT FORTRAN_EXTRA_FLAGS)
  SET(FORTRAN_EXTRA_FLAGS "")
ENDIF(NOT FORTRAN_EXTRA_FLAGS)

#################################################################
# STATIC LIBRARIES
#################################################################

# If shared libs not forced, prepend ".a" extension to library suffixes. 
# Useful to find first static libraries in future calls to FIND_PACKAGE
IF(NOT ${BUILD_SHARED_LIBS})
    SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a ${CMAKE_FIND_LIBRARY_SUFFIXES}")
ENDIF()

#################################################################
# Add external projects
#################################################################

# List of external projects to be managed
SET(EXT_PROJS thirdparty FPL)  
include(ExternalProject)

# Iterate over the list of external projects and include them
FOREACH(EXT_PROJ ${EXT_PROJS})

    SET(${EXT_PROJ}_BINARY_PATH ${PROJECT_BINARY_DIR}/${EXT_PROJ})

    IF(${EXT_PROJ} STREQUAL "FPL")
        SET(${EXT_PROJ}_SRC_PATH ${SRC_PATH})
        EXTERNALPROJECT_Add(${EXT_PROJ}
            PREFIX ${EXT_PROJ}
            STAMP_DIR ${EXT_PROJ}/src/${EXT_PROJ}-stamp
            DEPENDS thirdparty
            STEP_TARGETS configure build 
            DOWNLOAD_COMMAND ""
            SOURCE_DIR ${${EXT_PROJ}_SRC_PATH}
            BINARY_DIR ${${EXT_PROJ}_BINARY_PATH}
            INSTALL_DIR ${INSTALL_PREFIX}
            CONFIGURE_COMMAND ${CMAKE_COMMAND} -DBUILD_SHARED_LIBS=${BUILD_SHARED_LIBS} -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER} -DFORTRAN_EXTRA_FLAGS=${FORTRAN_EXTRA_FLAGS} -DCMAKE_VERBOSE_MAKEFILE=${CMAKE_VERBOSE_MAKEFILE} -DFPL_THIRDPARTY_DIR=${PROJECT_BINARY_DIR}/thirdparty -D${PROJECT_NAME}_ENABLE_TESTS=${${PROJECT_NAME}_ENABLE_TESTS} ${${EXT_PROJ}_SRC_PATH}
            BUILD_COMMAND ${CMAKE_COMMAND} --build ${${EXT_PROJ}_BINARY_PATH}
            TEST_COMMAND ""
            INSTALL_COMMAND ""
        )
        # Add Preconfigure step to force Configure-Build-Test-Install steps for Fempar (p.e., every time we call to $ make)
        EXTERNALPROJECT_Add_Step(${EXT_PROJ} PreConfigure
	        COMMAND ${CMAKE_COMMAND} -E touch ${EXT_PROJ}/src/${EXT_PROJ}-stamp/${EXT_PROJ}-PreConfigure-Fake.stamp
            DEPENDEES update
            DEPENDERS configure
            ALWAYS 1
        )
        # Add a custom target (FEMPAR-clean) to clean Fempar
        ADD_CUSTOM_TARGET(${EXT_PROJ}-clean
            COMMAND ${CMAKE_COMMAND} --build ${PROJECT_BINARY_DIR} --target clean 
            COMMAND ${CMAKE_COMMAND} --build ${${EXT_PROJ}_BINARY_PATH} --target clean 
        )
    ELSEIF(${EXT_PROJ} STREQUAL "thirdparty")
        SET(${EXT_PROJ}_SRC_PATH ${THIRDPARTY_PATH}) # Define ThirdParty source/root path (IN)
        # Add FEMPAR external project
        EXTERNALPROJECT_Add(${EXT_PROJ}
            PREFIX ${EXT_PROJ}
            STEP_TARGETS configure build
            DOWNLOAD_COMMAND ""
            SOURCE_DIR ${${EXT_PROJ}_SRC_PATH}
            BINARY_DIR ${${EXT_PROJ}_BINARY_PATH}
            INSTALL_DIR ${INSTALL_PREFIX}
            CONFIGURE_COMMAND ${CMAKE_COMMAND} -DBUILD_SHARED_LIBS=${BUILD_SHARED_LIBS} -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER} -DFORTRAN_EXTRA_FLAGS=${FORTRAN_EXTRA_FLAGS} -D${PROJECT_NAME}_THIRDPARTY_ENABLE_TESTS=${${PROJECT_NAME}_ENABLE_TESTS} ${${EXT_PROJ}_SRC_PATH}
            BUILD_COMMAND ${CMAKE_COMMAND} --build ${${EXT_PROJ}_BINARY_PATH}
            TEST_COMMAND ""
            INSTALL_COMMAND ""
        )
        # Add a custom target (THIRDPARTY-clean) to clean ThirdParty libraries
        ADD_CUSTOM_TARGET(${EXT_PROJ}-clean
            COMMAND ${CMAKE_COMMAND} --build ${PROJECT_BINARY_DIR} --target clean 
            COMMAND ${CMAKE_COMMAND} --build ${${EXT_PROJ}_BINARY_PATH} --target clean 
        )
    ENDIF()

    SET(${EXT_PROJ}_DIR ${${EXT_PROJ}_BINARY_PATH})

ENDFOREACH()

ADD_CUSTOM_TARGET(test
    COMMAND ${CMAKE_COMMAND} --build ${FPL_BINARY_PATH} --target test 
)


