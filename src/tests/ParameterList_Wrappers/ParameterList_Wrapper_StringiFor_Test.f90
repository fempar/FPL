Program ParameterListEntryContainer_Test

USE iso_fortran_env, only: OUTPUT_UNIT
USE PENF, only: I1P, I2P, I4P, I8P, R4P, R8P, str
USE FPL

type(ParameterList_t)     :: Parameters
type(ParameterList_t)     :: ParametersCopy
integer(I4P),allocatable  :: ValShape(:)
logical                   :: IsOfDataType = .false.
type(string), allocatable :: val0D
type(string), allocatable :: val1D(:)
type(string), allocatable :: val2D(:,:)
type(string), allocatable :: val3D(:,:,:)
type(string), allocatable :: val4D(:,:,:,:)
type(string), allocatable :: val5D(:,:,:,:,:)
type(string), allocatable :: val6D(:,:,:,:,:,:)
type(string), allocatable :: val7D(:,:,:,:,:,:,:)

allocate(val0D)
allocate(val1D(2))
allocate(val2D(1,2))
allocate(val3D(1,2,1))
allocate(val4D(1,2,1,1))
allocate(val5D(1,2,1,1,1))
allocate(val6D(1,2,1,1,1,1))
allocate(val7D(1,2,1,1,1,1,1))

val0D = string('A')
val1D(1) = string('A')
val1D(2) = string('ABC')
val2D(1,1:2) = val1D
val3D(1,1:2,1) = val1D
val4D(1,1:2,1,1) = val1D
val5D(1,1:2,1,1,1) = val1D
val6D(1,1:2,1,1,1,1) = val1D
val7D(1,1:2,1,1,1,1,1) = val1D

call FPL_Init()

call Parameters%Init(Size=3)

write(unit=OUTPUT_UNIT, fmt='(A)') 'Setting Values ...'

if(Parameters%Set(Key='0D',  Value=val0D) /= 0) stop -1
if(Parameters%Set(Key='1D',  Value=val1D) /= 0) stop -1
if(Parameters%Set(Key='2D',  Value=val2D) /= 0) stop -1
if(Parameters%Set(Key='3D',  Value=val3D) /= 0) stop -1
if(Parameters%Set(Key='4D',  Value=val4D) /= 0) stop -1
if(Parameters%Set(Key='5D',  Value=val5D) /= 0) stop -1
if(Parameters%Set(Key='6D',  Value=val6D) /= 0) stop -1
if(Parameters%Set(Key='7D',  Value=val7D) /= 0) stop -1

write(unit=OUTPUT_UNIT, fmt='(A)') ''
write(unit=OUTPUT_UNIT, fmt='(A)') 'Checking if nodes have been created ...'

if(.not. Parameters%isPresent(Key='0D'))  stop -1
if(.not. Parameters%isPresent(Key='1D'))  stop -1
if(.not. Parameters%isPresent(Key='2D'))  stop -1
if(.not. Parameters%isPresent(Key='3D'))  stop -1
if(.not. Parameters%isPresent(Key='4D'))  stop -1
if(.not. Parameters%isPresent(Key='5D'))  stop -1
if(.not. Parameters%isPresent(Key='6D'))  stop -1
if(.not. Parameters%isPresent(Key='7D'))  stop -1

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
call Parameters%Print(unit=OUTPUT_UNIT)
write(unit=OUTPUT_UNIT, fmt='(A,I4)') ' Parameter List Length: ',Parameters%Length()
if(Parameters%Length() /= 8) stop -1

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
write(unit=OUTPUT_UNIT, fmt='(A)') 'Checking Data Types ...'

IsOfDataType = Parameters%isOfDataType(Key='0D',   Mold=val0D)
write(unit=OUTPUT_UNIT, fmt=*) '0D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1
IsOfDataType = Parameters%isOfDataType(Key='1D',   Mold=val1D(1))
write(unit=OUTPUT_UNIT, fmt=*) '1D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1
IsOfDataType = Parameters%isOfDataType(Key='2D',   Mold=val2D(1,1))
write(unit=OUTPUT_UNIT, fmt=*) '2D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1
IsOfDataType = Parameters%isOfDataType(Key='3D',   Mold=val3D(1,1,1))
write(unit=OUTPUT_UNIT, fmt=*) '3D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1 
IsOfDataType = Parameters%isOfDataType(Key='4D',   Mold=val4D(1,1,1,1))
write(unit=OUTPUT_UNIT, fmt=*) '4D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1 
IsOfDataType = Parameters%isOfDataType(Key='5D',   Mold=val5D(1,1,1,1,1))
write(unit=OUTPUT_UNIT, fmt=*) '5D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1 
IsOfDataType = Parameters%isOfDataType(Key='6D',   Mold=val6D(1,1,1,1,1,1))
write(unit=OUTPUT_UNIT, fmt=*) '6D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1 
IsOfDataType = Parameters%isOfDataType(Key='7D',   Mold=val7D(1,1,1,1,1,1,1))
write(unit=OUTPUT_UNIT, fmt=*) '7D isOfDataType:', IsOfDataType; if(.not. IsOfDataType) stop -1 

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
write(unit=OUTPUT_UNIT, fmt='(A)') 'Checking shapes ...'

if(Parameters%GetShape(Key='0D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '0D Shape:',  ValShape
if(all(ValShape /= 0)) stop -1
if(Parameters%GetShape(Key='1D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '1D Shape:',  ValShape
if(all(ValShape /= shape(val1D))) stop -1
if(Parameters%GetShape(Key='2D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '2D Shape:',  ValShape
if(all(ValShape /= shape(val2D))) stop -1
if(Parameters%GetShape(Key='3D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '3D Shape:',  ValShape
if(all(ValShape /= shape(val3D))) stop -1
if(Parameters%GetShape(Key='4D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '4D Shape:',  ValShape
if(all(ValShape /= shape(val4D))) stop -1
if(Parameters%GetShape(Key='5D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '5D Shape:',  ValShape
if(all(ValShape /= shape(val5D))) stop -1
if(Parameters%GetShape(Key='6D',  shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '6D Shape:',  ValShape
if(all(ValShape /= shape(val6D))) stop -1
if(Parameters%GetShape(Key='7D',    shape=ValShape) /= 0) stop -1
write(unit=OUTPUT_UNIT, fmt='(A,7I4)') '7D Shape:',  ValShape
if(all(ValShape /= shape(val7D))) stop -1

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
write(unit=OUTPUT_UNIT, fmt='(A)') 'Can be assigned? ...'

if(.not. Parameters%isAssignable(Key='0D',  Value=val0D))  stop -1
if(.not. Parameters%isAssignable(Key='1D',  Value=val1D))  stop -1
if(.not. Parameters%isAssignable(Key='2D',  Value=val2D))  stop -1
if(.not. Parameters%isAssignable(Key='3D',  Value=val3D))  stop -1
if(.not. Parameters%isAssignable(Key='4D',  Value=val4D))  stop -1
if(.not. Parameters%isAssignable(Key='5D',  Value=val5D))  stop -1
if(.not. Parameters%isAssignable(Key='6D',  Value=val6D))  stop -1
if(.not. Parameters%isAssignable(Key='7D',  Value=val7D))  stop -1

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
write(unit=OUTPUT_UNIT, fmt='(A)') 'Getting Values ...'

if(Parameters%Get(Key='0D',  Value=val0D) /= 0) stop -1
if(Parameters%Get(Key='1D',  Value=val1D) /= 0) stop -1
if(Parameters%Get(Key='2D',  Value=val2D) /= 0) stop -1
if(Parameters%Get(Key='3D',  Value=val3D) /= 0) stop -1
if(Parameters%Get(Key='4D',  Value=val4D) /= 0) stop -1
if(Parameters%Get(Key='5D',  Value=val5D) /= 0) stop -1
if(Parameters%Get(Key='6D',  Value=val6D) /= 0) stop -1
if(Parameters%Get(Key='7D',  Value=val7D) /= 0) stop -1

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
write(unit=OUTPUT_UNIT, fmt='(A)') 'Copying parameter list ...'
ParametersCopy = Parameters

write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
write(unit=OUTPUT_UNIT, fmt='(A)') 'Deleting entries ...'

call Parameters%Del(Key='0D')
call Parameters%Del(Key='1D')
call Parameters%Del(Key='2D')
call Parameters%Del(Key='3D')
call Parameters%Del(Key='4D')
call Parameters%Del(Key='5D')
call Parameters%Del(Key='6D')
call Parameters%Del(Key='7D')
call Parameters%Print(unit=OUTPUT_UNIT)
write(unit=OUTPUT_UNIT, fmt='(A,I4)') ' Parameter List Length: ',Parameters%Length()
if(Parameters%Length() /= 0) stop -1


write(unit=OUTPUT_UNIT, fmt='(A)') 'Ok'
call Parameters%Print(unit=OUTPUT_UNIT)

call ParametersCopy%Free()

call Parameters%Free()

call FPL_Finalize()

end Program
