program WrapperTheWrapperFactoryList_Test

USE iso_fortran_env, only: OUTPUT_UNIT
USE PENF, only: I4P
USE WrapperFactoryListSingleton
USE WrapperFactory
USE DimensionsWrapper
USE FPL
implicit none

class(WrapperFactory_t),    pointer :: factory
class(DimensionsWrapper_t), pointer :: wrapper
type(string)                    :: val0D
type(string)                    :: val1D(2)
type(string)                    :: val2D(1,2)
type(string)                    :: val3D(1,2,1)
type(string)                    :: val4D(1,2,1,1)
type(string)                    :: val5D(1,2,1,1,1)
type(string)                    :: val6D(1,2,1,1,1,1)
type(string)                    :: val7D(1,2,1,1,1,1,1)

val0D = string('A')
val1D(1) = string('A')
val1D(2) = string('ABC')
val2D(1,1:2) = val1D
val3D(1,1:2,1) = val1D
val4D(1,1:2,1,1) = val1D
val5D(1,1:2,1,1,1) = val1D
val6D(1,1:2,1,1,1,1) = val1D
val7D(1,1:2,1,1,1,1,1) = val1D


call FPL_Init()
call TheWrapperFactoryList%Print(unit=OUTPUT_UNIT)

factory => TheWrapperFactoryList%GetFactory(Value=val0D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val0D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory)
end if

factory => TheWrapperFactoryList%GetFactory(Value=val1D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val1D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
end if

factory => TheWrapperFactoryList%GetFactory(Value=val2D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val2D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
endif

factory => TheWrapperFactoryList%GetFactory(Value=val3D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val3D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
endif

factory => TheWrapperFactoryList%GetFactory(Value=val4D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val4D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
endif

factory => TheWrapperFactoryList%GetFactory(Value=val5D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val5D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
endif

factory => TheWrapperFactoryList%GetFactory(Value=val6D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val6D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
endif

factory => TheWrapperFactoryList%GetFactory(Value=val7D)
if(associated(factory)) then
   wrapper => factory%Wrap(Value=val7D)
   if(associated(wrapper)) then
        call Wrapper%Print(unit=OUTPUT_UNIT)
        call wrapper%Free()
        deallocate(wrapper)
   endif
   nullify(factory) 
endif

call FPL_Finalize()

end program WrapperTheWrapperFactoryList_Test


