module StringiForWrapperFactory

USE StringiFor                  !< USE the data type to store
USE StringiForWrapper0D         !< USE the corresponding Wrapper 0D
USE StringiForWrapper1D         !< USE the corresponding Wrapper 1D
USE StringiForWrapper2D         !< USE the corresponding Wrapper 2D
USE StringiForWrapper3D         !< USE the corresponding Wrapper 3D
USE StringiForWrapper4D         !< USE the corresponding Wrapper 4D
USE StringiForWrapper5D         !< USE the corresponding Wrapper 5D
USE StringiForWrapper6D         !< USE the corresponding Wrapper 6D
USE StringiForWrapper7D         !< USE the corresponding Wrapper 7D
USE DimensionsWrapper           !< USE the DimensionsWrapper abstract class
USE WrapperFactory              !< USE the WrapperFactory abstract class
USE ErrorMessages               !< USE the ErrorMessages for printing error messages
USE PENF, only: I1P             !< USE I1P data type

implicit none
private

    type, extends(WrapperFactory_t) :: StringiForWrapperFactory_t
    private

    contains
        procedure         :: Wrap0D      => StringiForWrapperFactory_Wrap0D         !< Wraps scalar string
        procedure         :: Wrap1D      => StringiForWrapperFactory_Wrap1D         !< Wraps 1D arrays of string
        procedure         :: Wrap2D      => StringiForWrapperFactory_Wrap2D         !< Wraps 2D arrays of string
        procedure         :: Wrap3D      => StringiForWrapperFactory_Wrap3D         !< Wraps 3D arrays of string
        procedure         :: Wrap4D      => StringiForWrapperFactory_Wrap4D         !< Wraps 4D arrays of string
        procedure         :: Wrap5D      => StringiForWrapperFactory_Wrap5D         !< Wraps 5D arrays of string
        procedure         :: Wrap6D      => StringiForWrapperFactory_Wrap6D         !< Wraps 6D arrays of string
        procedure         :: Wrap7D      => StringiForWrapperFactory_Wrap7D         !< Wraps 7D arrays of string
        procedure, public :: hasSameType => StringiForWrapperFactory_hasSameType    !< Check if the data type of a input Mold is string
    end type

    type(StringiForWrapperFactory_t), public :: WrapperFactoryStringiFor            !< Public Wrapper Factory (singleton)

contains

    function StringiForWrapperFactory_hasSameType(this, Value) result(hasSameType)
    !-----------------------------------------------------------------
    !< Check if Value type agrees with wrapper type
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t), intent(IN) :: this
        class(*),                          intent(IN) :: Value
        logical                                       :: hasSameType
    !-----------------------------------------------------------------
        hasSameType = .false.
        select type(Value)
            type is (string)
                hasSameType = .true.
        end select
    end function StringiForWrapperFactory_hasSameType


    function StringiForWrapperFactory_Wrap0D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 0D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value)) then
            allocate(StringiForWrapper0D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=0_I1P)
            select type (Wrapper)
                type is(StringiForWrapper0D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap0D


    function StringiForWrapperFactory_Wrap1D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 1D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1))) then
            allocate(StringiForWrapper1D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=1_I1P)
            select type (Wrapper)
                type is(StringiForWrapper1D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap1D


    function StringiForWrapperFactory_Wrap2D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 2D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:,1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1,1))) then
            allocate(StringiForWrapper2D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=2_I1P)
            select type (Wrapper)
                type is(StringiForWrapper2D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap2D


    function StringiForWrapperFactory_Wrap3D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 3D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:,1:,1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1,1,1))) then
            allocate(StringiForWrapper3D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=3_I1P)
            select type (Wrapper)
                type is(StringiForWrapper3D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap3D


    function StringiForWrapperFactory_Wrap4D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 4D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:,1:,1:,1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1,1,1,1))) then
            allocate(StringiForWrapper4D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=4_I1P)
            select type (Wrapper)
                type is(StringiForWrapper4D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap4D


    function StringiForWrapperFactory_Wrap5D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 5D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:,1:,1:,1:,1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1,1,1,1,1))) then
            allocate(StringiForWrapper5D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=5_I1P)
            select type (Wrapper)
                type is(StringiForWrapper5D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap5D


    function StringiForWrapperFactory_Wrap6D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 6D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:,1:,1:,1:,1:,1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1,1,1,1,1,1))) then
            allocate(StringiForWrapper6D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=6_I1P)
            select type (Wrapper)
                type is(StringiForWrapper6D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap6D


    function StringiForWrapperFactory_Wrap7D(this, Value) result(Wrapper)
    !-----------------------------------------------------------------
    !< Create String 7D Wrapper
    !-----------------------------------------------------------------
        implicit none
        class(StringiForWrapperFactory_t),       intent(IN)    :: this
        class(*),                                intent(IN)    :: Value(1:,1:,1:,1:,1:,1:,1:)
        class(DimensionsWrapper_t), pointer                    :: Wrapper
    !-----------------------------------------------------------------
        if(this%hasSameType(Value(1,1,1,1,1,1,1))) then
            allocate(StringiForWrapper7D_t::Wrapper)
            call Wrapper%SetDimensions(Dimensions=7_I1P)
            select type (Wrapper)
                type is(StringiForWrapper7D_t)
                    call Wrapper%Set(Value=Value)
            end select
        endif
    end function StringiForWrapperFactory_Wrap7D

end module StringiForWrapperFactory
