!-----------------------------------------------------------------
! FPL (Fortran Parameter List)
! Copyright (c) 2015 Santiago Badia, Alberto F. Martín, 
! Javier Principe and Víctor Sande.
! All rights reserved.
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 3.0 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public
! License along with this library.
!-----------------------------------------------------------------

module StringiForWrapper1D

USE StringiFor                      !< USE the StringiFor
USE DimensionsWrapper1D
USE PENF, only: I4P, str, byte_size
USE ErrorMessages

implicit none
private

    type, extends(DimensionsWrapper1D_t) :: StringiForWrapper1D_t
        type(string), allocatable :: Value(:)
    contains
    private
        procedure, public :: Set            => StringiForWrapper1D_Set
        procedure, public :: Get            => StringiForWrapper1D_Get
        procedure, public :: GetShape       => StringiForWrapper1D_GetShape
        procedure, public :: GetPolymorphic => StringiForWrapper1D_GetPolymorphic
        procedure, public :: GetPointer     => StringiForWrapper1D_GetPointer
        procedure, public :: DataSizeInBytes=> StringiForWrapper1D_DataSizeInBytes
        procedure, public :: isOfDataType   => StringiForWrapper1D_isOfDataType
        procedure, public :: toString       => StringiForWrapper1D_toString
        procedure, public :: Free           => StringiForWrapper1D_Free
        procedure, public :: Print          => StringiForWrapper1D_Print
        final             ::                   StringiForWrapper1D_Final
    end type           

public :: StringiForWrapper1D_t

contains


    subroutine StringiForWrapper1D_Final(this) 
    !-----------------------------------------------------------------
    !< Final procedure of DimensionsWrapper1D
    !-----------------------------------------------------------------
        type(StringiForWrapper1D_t), intent(INOUT) :: this
    !-----------------------------------------------------------------
        call this%Free()
    end subroutine


    subroutine StringiForWrapper1D_Set(this, Value) 
    !-----------------------------------------------------------------
    !< Set String Wrapper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(INOUT) :: this
        class(*),                         intent(IN)    :: Value(:)
        integer                                         :: err
    !-----------------------------------------------------------------
        select type (Value)
            type is (string)
                allocate(this%Value(size(Value,dim=1)), stat=err)
				this%Value = Value
                if(err/=0) &
                    call msg%Error( txt='Setting Value: Allocation error ('//&
                                    str(no_sign=.true.,n=err)//')', &
                                    file=__FILE__, line=__LINE__ )
            class Default
                call msg%Warn( txt='Setting value: Expected data type (String)', &
                               file=__FILE__, line=__LINE__ )
        end select
    end subroutine


    subroutine StringiForWrapper1D_Get(this, Value) 
    !-----------------------------------------------------------------
    !< Get String Wrapper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(IN)  :: this
        class(*),                         intent(OUT) :: Value(:)
        integer(I4P), allocatable                     :: ValueShape(:)
    !-----------------------------------------------------------------
        select type (Value)
            type is (string)
                call this%GetShape(ValueShape)
                if(all(ValueShape == shape(Value))) then
                    Value = this%Value
                else
                    call msg%Warn(txt='Getting value: Wrong shape ('//&
                                  str(no_sign=.true.,n=ValueShape)//'/='//&
                                  str(no_sign=.true.,n=shape(Value))//')',&
                                  file=__FILE__, line=__LINE__ )
                endif
            class Default
                call msg%Warn(txt='Getting value: Expected data type (String)',&
                              file=__FILE__, line=__LINE__ )
        end select
    end subroutine


    subroutine StringiForWrapper1D_GetShape(this, ValueShape)
    !-----------------------------------------------------------------
    !< Get Wrapper Value Shape
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(IN)    :: this
        integer(I4P), allocatable,        intent(INOUT) :: ValueShape(:)
    !-----------------------------------------------------------------
        if(allocated(ValueShape)) deallocate(ValueShape)
		allocate(ValueShape(this%GetDimensions()))
        ValueShape = shape(this%Value, kind=I4P)
    end subroutine


    function StringiForWrapper1D_GetPointer(this) result(Value) 
    !-----------------------------------------------------------------
    !< Get Unlimited Polymorphic W2apper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t), target, intent(IN)  :: this
        class(*), pointer                                 :: Value(:)
    !-----------------------------------------------------------------
        Value => this%value
    end function


    subroutine StringiForWrapper1D_GetPolymorphic(this, Value) 
    !-----------------------------------------------------------------
    !< Get Unlimited Polymorphic Wrapper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),      intent(IN)  :: this
        class(*), allocatable,            intent(OUT) :: Value(:)
    !-----------------------------------------------------------------
        allocate(Value(size(this%Value,dim=1)),source=this%Value)
    end subroutine


    subroutine StringiForWrapper1D_Free(this) 
    !-----------------------------------------------------------------
    !< Free a DimensionsWrapper1D
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(INOUT) :: this
        integer                                         :: err
    !-----------------------------------------------------------------
        if(allocated(this%Value)) then
            deallocate(this%Value, stat=err)
            if(err/=0) call msg%Error(txt='Freeing Value: Deallocation error ('// &
                                      str(no_sign=.true.,n=err)//')',             &
                                      file=__FILE__, line=__LINE__ )
        endif
    end subroutine


    function StringiForWrapper1D_DataSizeInBytes(this) result(DataSizeInBytes)
    !-----------------------------------------------------------------
    !< Return the size of the stored data in bytes
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(IN) :: this            !< Dimensions wrapper 1D
        integer(I4P)                                 :: DataSizeInBytes !< Size in bytes of the stored data
    !-----------------------------------------------------------------
        DataSizeInBytes = 0
        DataSizeInBytes = sizeof(this%value)
    end function StringiForWrapper1D_DataSizeInBytes


    function StringiForWrapper1D_isOfDataType(this, Mold) result(isOfDataType)
    !-----------------------------------------------------------------
    !< Check if Mold and Value are of the same datatype 
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(IN) :: this           !< Dimensions wrapper 1D
        class(*),                         intent(IN) :: Mold           !< Mold for data type comparison
        logical                                      :: isOfDataType   !< Boolean flag to check if Value is of the same data type as Mold
    !-----------------------------------------------------------------
        isOfDataType = .false.
        select type (Mold)
            type is (string)
                isOfDataType = .true.
        end select
    end function StringiForWrapper1D_isOfDataType


    subroutine StringiForWrapper1D_toString(this, String, Separator) 
    !-----------------------------------------------------------------
    !< Return the wrapper value as a string
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(IN)    :: this
        character(len=:), allocatable,    intent(INOUT) :: String
        character(len=1), optional,       intent(IN)    :: Separator
        character(len=1)                                :: Sep
        integer(I4P)                                    :: idx
    !-----------------------------------------------------------------
        String = ''; Sep = ','; if(present(Separator)) Sep = Separator
        if(allocated(this%Value)) then
            do idx=1, size(this%Value)-1
                String = String // trim(this%Value(idx)%chars()) // Sep
            enddo
            String = String // trim(this%Value(size(this%Value))%chars())
        endif
    end subroutine


    subroutine StringiForWrapper1D_Print(this, unit, prefix, iostat, iomsg)
    !-----------------------------------------------------------------
    !< Print Wrapper
    !-----------------------------------------------------------------
        class(StringiForWrapper1D_t),     intent(IN)  :: this         !< DimensionsWrapper
        integer(I4P),                     intent(IN)  :: unit         !< Logic unit.
        character(*), optional,           intent(IN)  :: prefix       !< Prefixing string.
        integer(I4P), optional,           intent(OUT) :: iostat       !< IO error.
        character(*), optional,           intent(OUT) :: iomsg        !< IO error message.
        character(len=:), allocatable                 :: prefd        !< Prefixing string.
        character(len=:), allocatable                 :: strvalue     !< String value
        integer(I4P)                                  :: iostatd      !< IO error.
        character(500)                                :: iomsgd       !< Temporary variable for IO error message.
    !-----------------------------------------------------------------
        prefd = '' ; if (present(prefix)) prefd = prefix
        call this%toString(strvalue)
        write(unit=unit,fmt='(A)',iostat=iostatd,iomsg=iomsgd) prefd//' Data Type = String'//&
                            ', Dimensions = '//trim(str(no_sign=.true., n=this%GetDimensions()))//&
                            ', Bytes = '//trim(str(no_sign=.true., n=this%DataSizeInBytes()))//&
                            ', Value = '//strvalue
        if (present(iostat)) iostat = iostatd
        if (present(iomsg))  iomsg  = iomsgd
    end subroutine StringiForWrapper1D_Print

end module StringiForWrapper1D
