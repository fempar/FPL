!-----------------------------------------------------------------
! FPL (Fortran Parameter List)
! Copyright (c) 2015 Santiago Badia, Alberto F. Martín, 
! Javier Principe and Víctor Sande.
! All rights reserved.
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 3.0 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public
! License along with this library.
!-----------------------------------------------------------------

module StringiForWrapper5D

USE StringiFor                      !< USE the StringiFor
USE DimensionsWrapper5D
USE PENF, only: I4P, str, byte_size
USE ErrorMessages

implicit none
private

    type, extends(DimensionsWrapper5D_t) :: StringiForWrapper5D_t
        type(string), allocatable :: Value(:,:,:,:,:)
    contains
    private
        procedure, public :: Set            => StringiForWrapper5D_Set
        procedure, public :: Get            => StringiForWrapper5D_Get
        procedure, public :: GetShape       => StringiForWrapper5D_GetShape
        procedure, public :: GetPolymorphic => StringiForWrapper5D_GetPolymorphic
        procedure, public :: GetPointer     => StringiForWrapper5D_GetPointer
        procedure, public :: DataSizeInBytes=> StringiForWrapper5D_DataSizeInBytes
        procedure, public :: isOfDataType   => StringiForWrapper5D_isOfDataType
        procedure, public :: toString       => StringiForWrapper5D_toString
        procedure, public :: Free           => StringiForWrapper5D_Free
        procedure, public :: Print          => StringiForWrapper5D_Print
        final             ::                   StringiForWrapper5D_Final
    end type           

public :: StringiForWrapper5D_t

contains


    subroutine StringiForWrapper5D_Final(this) 
    !-----------------------------------------------------------------
    !< Final procedure of DimensionsWrapper5D
    !-----------------------------------------------------------------
        type(StringiForWrapper5D_t), intent(INOUT) :: this
    !-----------------------------------------------------------------
        call this%Free()
    end subroutine


    subroutine StringiForWrapper5D_Set(this, Value) 
    !-----------------------------------------------------------------
    !< Set String Wrapper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(INOUT) :: this
        class(*),                         intent(IN)    :: Value(:,:,:,:,:)
        integer                                         :: err
    !-----------------------------------------------------------------
        select type (Value)
            type is (string)
                allocate(this%Value(size(Value,dim=1),  &
                                    size(Value,dim=2),  &
                                    size(Value,dim=3),  &
                                    size(Value,dim=4),  &
                                    size(Value,dim=5)), &
                                    stat=err)
				this%Value = Value
                if(err/=0) &
                    call msg%Error( txt='Setting Value: Allocation error ('//&
                                    str(no_sign=.true.,n=err)//')', &
                                    file=__FILE__, line=__LINE__ )
            class Default
                call msg%Warn( txt='Setting value: Expected data type (String)', &
                               file=__FILE__, line=__LINE__ )
        end select
    end subroutine


    subroutine StringiForWrapper5D_Get(this, Value) 
    !-----------------------------------------------------------------
    !< Get String Wrapper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN)  :: this
        class(*),                         intent(OUT) :: Value(:,:,:,:,:)
        integer(I4P), allocatable                     :: ValueShape(:)
    !-----------------------------------------------------------------
        select type (Value)
            type is (string)
                call this%GetShape(ValueShape)
                if(all(ValueShape == shape(Value))) then
                    Value = this%Value
                else
                    call msg%Warn(txt='Getting value: Wrong shape ('//&
                                  str(no_sign=.true.,n=ValueShape)//'/='//&
                                  str(no_sign=.true.,n=shape(Value))//')',&
                                  file=__FILE__, line=__LINE__ )
                endif
            class Default
                call msg%Warn(txt='Getting value: Expected data type (String)',&
                              file=__FILE__, line=__LINE__ )
        end select
    end subroutine


    subroutine StringiForWrapper5D_GetShape(this, ValueShape)
    !-----------------------------------------------------------------
    !< Get Wrapper Value Shape
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN)    :: this
        integer(I4P), allocatable,        intent(INOUT) :: ValueShape(:)
    !-----------------------------------------------------------------
        if(allocated(ValueShape)) deallocate(ValueShape)
		allocate(ValueShape(this%GetDimensions()))
        ValueShape = shape(this%Value, kind=I4P)
    end subroutine


    function StringiForWrapper5D_GetPointer(this) result(Value) 
    !-----------------------------------------------------------------
    !< Get Unlimited Polymorphic W2apper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t), target, intent(IN)  :: this
        class(*), pointer                                 :: Value(:,:,:,:,:)
    !-----------------------------------------------------------------
        Value => this%value
    end function


    subroutine StringiForWrapper5D_GetPolymorphic(this, Value) 
    !-----------------------------------------------------------------
    !< Get Unlimited Polymorphic Wrapper Value
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN)  :: this
        class(*), allocatable,            intent(OUT) :: Value(:,:,:,:,:)
    !-----------------------------------------------------------------
        allocate(Value(size(this%Value,dim=1),  &
                       size(this%Value,dim=2),  &
                       size(this%Value,dim=3),  &
                       size(this%Value,dim=4),  &
                       size(this%Value,dim=5)), &
                       source=this%Value)
    end subroutine


    subroutine StringiForWrapper5D_Free(this) 
    !-----------------------------------------------------------------
    !< Free a DimensionsWrapper5D
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(INOUT) :: this
        integer                                         :: err
    !-----------------------------------------------------------------
        if(allocated(this%Value)) then
            deallocate(this%Value, stat=err)
            if(err/=0) call msg%Error(txt='Freeing Value: Deallocation error ('// &
                                      str(no_sign=.true.,n=err)//')',             &
                                      file=__FILE__, line=__LINE__ )
        endif
    end subroutine


    function StringiForWrapper5D_DataSizeInBytes(this) result(DataSizeInBytes)
    !-----------------------------------------------------------------
    !< Return the size of the stored data in bytes
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN) :: this            !< Dimensions wrapper 5D
        integer(I4P)                                 :: DataSizeInBytes !< Size in bytes of the stored data
        integer                                      :: i
    !-----------------------------------------------------------------
        DataSizeInBytes = 0
        DataSizeInBytes = sizeof(this%value)
    end function StringiForWrapper5D_DataSizeInBytes


    function StringiForWrapper5D_isOfDataType(this, Mold) result(isOfDataType)
    !-----------------------------------------------------------------
    !< Check if Mold and Value are of the same datatype 
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN) :: this           !< Dimensions wrapper 5D
        class(*),                         intent(IN) :: Mold           !< Mold for data type comparison
        logical                                      :: isOfDataType   !< Boolean flag to check if Value is of the same data type as Mold
    !-----------------------------------------------------------------
        isOfDataType = .false.
        select type (Mold)
            type is (string)
                isOfDataType = .true.
        end select
    end function StringiForWrapper5D_isOfDataType


    subroutine StringiForWrapper5D_toString(this, String, Separator) 
    !-----------------------------------------------------------------
    !< Return the wrapper value as a string
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN)    :: this
        character(len=:), allocatable,    intent(INOUT) :: String
        character(len=1), optional,       intent(IN)    :: Separator
        character(len=1)                                :: Sep
        integer(I4P)                                    :: idx1, idx2, idx3, idx4, idx5
    !-----------------------------------------------------------------
        String = ''; Sep = ','; if(present(Separator)) Sep = Separator
        if(allocated(this%Value)) then
            do idx5=1, size(this%Value, dim=5)
                do idx4=1, size(this%Value, dim=4)
                    do idx3=1, size(this%Value, dim=3)
                        do idx2=1, size(this%Value, dim=2)
                            do idx1=1, size(this%Value, dim=1)
                                String = String // trim(this%Value(idx1,idx2,idx3,idx4,idx5)%chars()) // Sep
                            enddo
                        enddo
                    enddo
                enddo
            enddo
        endif
    end subroutine


    subroutine StringiForWrapper5D_Print(this, unit, prefix, iostat, iomsg)
    !-----------------------------------------------------------------
    !< Print Wrapper
    !-----------------------------------------------------------------
        class(StringiForWrapper5D_t),     intent(IN)  :: this         !< DimensionsWrapper
        integer(I4P),                     intent(IN)  :: unit         !< Logic unit.
        character(*), optional,           intent(IN)  :: prefix       !< Prefixing string.
        integer(I4P), optional,           intent(OUT) :: iostat       !< IO error.
        character(*), optional,           intent(OUT) :: iomsg        !< IO error message.
        character(len=:), allocatable                 :: prefd        !< Prefixing string.
        character(len=:), allocatable                 :: strvalue     !< String value
        integer(I4P)                                  :: iostatd      !< IO error.
        character(500)                                :: iomsgd       !< Temporary variable for IO error message.
    !-----------------------------------------------------------------
        prefd = '' ; if (present(prefix)) prefd = prefix
        call this%toString(strvalue)
        write(unit=unit,fmt='(A)',iostat=iostatd,iomsg=iomsgd) prefd//' Data Type = String'//&
                            ', Dimensions = '//trim(str(no_sign=.true., n=this%GetDimensions()))//&
                            ', Bytes = '//trim(str(no_sign=.true., n=this%DataSizeInBytes()))//&
                            ', Value = '//strvalue
        if (present(iostat)) iostat = iostatd
        if (present(iomsg))  iomsg  = iomsgd
    end subroutine StringiForWrapper5D_Print

end module StringiForWrapper5D
